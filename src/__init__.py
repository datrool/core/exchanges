# -*- coding: utf-8 -*-
from .binance import Binance
__version__ = "__VERSION__"

wrapper_list = {
	"binance": Binance
}


def load_exchange(name):
	if name in wrapper_list:
		return wrapper_list[name]
	else:
		raise NotImplementedError("no exchange named `%s`" % name)
