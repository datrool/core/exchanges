# datrool / exchanges

Exchange wrappers for the datrool project.

## Installation

Install package from PYPI repository
```sh
python -m pip install datrool-exchanges
```

## Contains
- [x] abstract exchange class
- [ ] exchange wrapper for strategy testing

### Wrapper implementations

#### Cryptocurrency spot exchanges
- [x] Binance
- [ ] BYBIT
- [ ] CoinBase
- [ ] MEXC
- [ ] Gate.io

#### Regular brokers
- [ ] Capital.com
- [ ] XTB

## License

Created by Patrik Katreňák and released under General Public License v3.0.
